declare
    l_qry    varchar2(4000) := NULL;
    l_op     varchar2(10) := '=';
begin

    l_qry := 'select title, author, text from messages';

    if :P6_COLUMN1 is not null then
        l_qry := l_qry || ' where ' || :P6_COLUMN1 || ' ' || 
                   :P6_OP1 || ' :P6_VALUE1 ';     
       
    end if;

    wwv_flow.debug(l_qry);

    return l_qry;
end;